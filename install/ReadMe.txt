Please refer to the LayerMakerInstructions.pdf document that has been copied into your macro folder.
You can access this document by selecting S9 from the macro main menu (you will require Adobe Reader (or similar) software to read it).

DataCAD 14 introduced polygons and slabs  with up to 256 sides and 3D curved surfaces with up to 128 divisions. Please especially refer to the notes on these in the Instructions document, as some aspects of this macro do not support these entities.

Bug reports or enhancement requests can be sent to dhsoftware1@gmail.com

Thank you,
David Henderson
dhsoftware1@gmail.com
www.dhsoftware.com.au