
library LayerMaker;

uses
  System.sysutils,
  //Sharemem, {Do not use. All DataCAD parameters are limited to short strings}

  //DCAL for Delphi Header Files
  UConstants in '../../Header Files/UConstants.pas',
  UInterfaces in '../../Header Files/UInterfaces.pas',
  UInterfacesRecords in '../../Header Files/UInterfacesRecords.pas',
  URecords in '../../Header Files/URecords.pas',
  UVariables in '../../Header Files/UVariables.pas';

{$E dmx}  //dmx is the extension for DCAL Dll's
{$R *.res}

const
   MacroVersion = '1.0.0.3';

//The values stored in the records you define here will be rememberd
//between dispatcher calls because they are pushed onto the command stack
type
  HelloL = record
    state: asint;
    zbase : double;
    case byte of
      0: (getp: getpointArg);
  end;
  PHelloL = ^HelloL;

//Main Function (Re-entrant State Machine)
function LayerMaker_main(act : action; pl, pargs : Pointer) : wantType;
//The values stored in variables you define here will not be retained
//between dispatcher calls because they are not pushed onto the stack
var
   retval : asint;
   l : PHelloL;
begin
   l := PHelloL(pl);

   {Section 3: Aagain - Return to menus and wait for input}
   if act = aagain then begin  { Re-entrant section }
      case l.state of { Case }
         1 :
         begin
            //If a key was pressed or a menu button was selected...
            if l.getp.Result = res_escape then begin
               case l.getp.key of
                  f1 : l.state := 2;  {Say Hello}

                  s9 : l.state := 3;  {Options 1 Sub-menu}

                  s0 : l.state := 0;  {Exit}
               end;  { Which key was pressed? }
            end;  { Was a key pressed or menu selected? }
         end;  { 1 }
         2 :
         begin
            l.state := 1
         end;  { 2 }
         3 :
         begin
            //If a key was pressed or a menu button was selected...
            if l.getp.Result = res_escape then begin
               case l.getp.key of
                  f1 : l.state := 3;  { Option 1.1 }
                  f2 : l.state := 3;  { Option 2.1}
                  f3 : l.state := 3;  { Option 3.1 }

                  s9 : l.state := 4;  { Options 2 }

                  s0 : l.state := 1;  {Exit}
               end;  { Which key was pressed? }
            end;  { Was a key pressed or menu selected? }
         end;  { 3 }
         4 :
         begin
            //If a key was pressed or a menu button was selected...
            if l.getp.Result = res_escape then begin
               case l.getp.key of
                  f1 : l.state := 4;  { Option 1.2 }
                  f2 : l.state := 4;  { Option 2.2 }
                  f3 : l.state := 4;  { Option 3.2 }

                  s9 : l.state := 5;  { Options 3 }

                  s0 : l.state := 3;  { Exit to previous menu }
               end;  { Which key was pressed? }
            end;  { Was a key pressed or menu selected? }
         end;  { 4 }
         5 :
         begin
            //If a key was pressed or a menu button was selected...
            if l.getp.Result = res_escape then begin
               case l.getp.key of
                  f1 : l.state := 5;  { Option 1.3 }
                  f2 : l.state := 5;  { Option 2.3 }
                  f3 : l.state := 5;  { Option 3.3 }

                  //s9 : l.state := 6;  { Options 4 }

                  s0 : l.state := 4;  { Exit to previous menu }
               end;  { Which key was pressed? }
            end;  { Was a key pressed or menu selected? }
         end;  { 5 }
         6 :
         begin
            //do something
         end;  { 6 }
      else
         l.state := 0;
      end; { Case }
   end;  { Re-entrant Section }

    {Section 2: Afirst - Tasks to perform only once}
    if act = Afirst then begin  //Initialize local state and other variables
      wrterr('Hello World Macro (DCAL for Delphi) - Version' + MacroVersion, true);
      l.state := 1;
      l.zbase := zbase;
      wrtmsg('Zbase = ' + FloatToStr(l.zbase));
      l.zbase := (1000/25.4)*36;
      zbase := l.zbase;
    end

    {Section 4: Alast - If your macro has been interrupted by user action}
    else if act = Alast then begin
      // If you're going to get blown off the stack...
      // (i.e.) the user pressed a hotkey,
      // then this is your last chance to clean up temporary data.
    end

    {Section 1: AlSize - Allocate memory on stack for local variables}
    else if act = AlSize then begin
      SetLocalSize(sizeof(l^));
    end;

    if act <> alsize then begin  { Action section - Invoke the Dispatcher }
      case l.state of
        1 :
        begin  { 1: Main Menu }
          wrtlvl('Hello');  //Set Menu Title
          lblsinit;
          lblset(1, 'Say Hello', 'Select this button to say "Hello"');

          lblset(19, 'Options 1', 'Options One Menu');

          //lblset(20, 'Exit', '"Goodbye"');
          lblson1;
          getpoint(l.getp, retval);
          {Dispatcher will call as soon as it has control, go to Aagain: 1}
        end;  { 1 }
        2:
        begin
          wrtmsg ('Hello World!', true);
          callNone(retval);{ You have to ask the Dispatcher to do something. }
                           { In this case, ask the dispatcher to do nothing. }
          {Dispatcher will call as soon as it has control, go to Aagain: 2}
        end;  { 2 }
        3:
        begin
          wrtlvl('Options 1');  //Set Menu Title
          lblsinit;
          lblset(1, 'Option 1.1', 'Set option one values');
          lblset(2, 'Option 2.1', 'Set option two values');
          lblset(3, 'Option 3.1', 'Set option three values');

          lblset(19, 'Options 2', 'Options Two Menu');

          //lblset(20, 'Exit', '"Goodbye"');
          lblson1;
          getpoint(l.getp, retval);
          {Dispatcher will call as soon as it has control, go to Aagain: 3}
        end;  { 3 }
        4:
        begin
          wrtlvl('Options 2');  //Set Menu Title
          lblsinit;
          lblset(1, 'Option 1.2', 'Set option one values');
          lblset(2, 'Option 2.2', 'Set option two values');
          lblset(3, 'Option 3.2', 'Set option three values');

          //lblset(19, 'Options 3', 'Options Two Menu');

          //lblset(20, 'Exit', '"Goodbye"');
          lblson1;
          getpoint(l.getp, retval);
          {Dispatcher will call as soon as it has control, go to Aagain: 4}
        end;  { 4 }
        5:
        begin
          //do something
          {Dispatcher will call as soon as it has control, go to Aagain: 5}
        end;  { 5 }
        6:
        begin
          //do something
          {Dispatcher will call as soon as it has control, go to Aagain: 6}
        end;  { 6 }
      else
        retval := XDone; //Don't go back to Aagain
      end; { Case }
    end; { Action section }
    Result := retval;
end; { LayerMaker_main }
//End Main Function (Re-entrant State Machine)

function Main(dcalstate : asint; act : action; pl, pargs : Pointer) : wantType; stdcall;
begin
   case dcalstate of
      XDcalStateBegin : Result := LayerMaker_main(act, pl, pargs);
         else
            Result := XDone;{Necessary}
   end;
end;
//End of Main Function (Re-entrant State Machine)

exports
   Main;{This is the entry function that will be called by DataCAD.
         All DCAL Dlls will have this function.}

begin

end.
